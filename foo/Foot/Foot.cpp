#include "Foot.h"
#include <iostream>

namespace foo {

Foot::Foot()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}
void Foot::foo()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}
void Foot::foot()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

} /* namespace foo */
