#pragma once

#include <Foo.h>

namespace foo {
class Foot : public Foo
{
  public:
    Foot();
    void foo() override;
    void foot();
};
} /* namespace foo */
