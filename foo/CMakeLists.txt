cmake_minimum_required(VERSION 3.4)

project(Foo VERSION 1.0.0)


option(ENABLE_FOOT "Enable Foot subdir" OFF)
option(ENABLE_FOOD "Enable Foot subdir" OFF)

set(Foo_SOURCES Foo.cpp)
set(Foo_HEADERS Foo.h)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})
add_library(foo STATIC "${Foo_SOURCES};${Foo_HEADERS}")
add_library(foo-shared SHARED "${Foo_SOURCES};${Foo_HEADERS}")
set_target_properties(foo-shared PROPERTIES OUTPUT_NAME "foo")

include(GNUInstallDirs)

#### Configuration ####
set(config_install_dir "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}")
set(generated_dir ${CMAKE_CURRENT_BINARY_DIR}/generated)
set(version_config ${generated_dir}/${PROJECT_NAME}Version.cmake)
set(project_config ${generated_dir}/${PROJECT_NAME}Config.cmake)
set(TARGETS_EXPORT_NAME "${PROJECT_NAME}Targets")
set(namespace "${PROJECT_NAME}::")

if(ENABLE_FOOT)
add_subdirectory(Foot)
endif()
if(ENABLE_FOOD)
add_subdirectory(Food)
endif()


include(CMakePackageConfigHelpers)

write_basic_package_version_file("${version_config}" COMPATIBILITY SameMajorVersion)

configure_package_config_file(
    "cmake/FooConfig.cmake.in"
    "${project_config}"
    INSTALL_DESTINATION "${config_install_dir}"
    )

install(
    TARGETS foo foo-shared
    EXPORT "${TARGETS_EXPORT_NAME}"
    LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    ARCHIVE DESTINATION "${CMAKE_INSTALL_LIBDIR}"
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
    INCLUDES DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
    )

install(
    FILES "${Foo_HEADERS}"
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}"
    )

install(
    FILES "${project_config}" "${version_config}"
    DESTINATION "${config_install_dir}"
    )

install(
    EXPORT "${TARGETS_EXPORT_NAME}"
    NAMESPACE "${namespace}"
    DESTINATION "${config_install_dir}"
    )

