#include "Food.h"
#include <iostream>

namespace foo {

Food::Food()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}
void Food::foo()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}
void Food::food()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

} /* namespace foo */
