#pragma once

#include <Foo.h>

namespace foo {
class Food : public Foo
{
  public:
    Food();
    void foo() override;
    void food();
};
} /* namespace foo */
