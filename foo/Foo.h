#pragma once

namespace foo {

class Foo
{
  public:
    Foo() = default;
    virtual void foo();
};

} /* namespace foo */
