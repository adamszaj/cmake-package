#include "Foo.h"
#include <iostream>

namespace foo {

void Foo::foo() {
    std::cout << "Foo::foo()" << std::endl;
}

} /* namespace foo */
