#pragma once

#include <Foo/Foo.h>

namespace bar {

class Bar
{
  public:
    Bar()
    {
        foo.foo();
    }
  private:
    foo::Foo foo;
};

} /* namespace bar */
