#!/bin/bash

SRC_DIR=$(dirname $(realpath $0))
TMP=${SRC_DIR}/tmp
INSTALL_DIR=${TMP}/install

mkdir -p ${TMP}
for project in foo bar buz ; do
    for build_type in Debug ; do
        BUILD_DIR=${TMP}/${project}-build-${build_type}
        mkdir -p ${BUILD_DIR}
        pushd ${BUILD_DIR}
        cmake -DCMAKE_BUILD_TYPE=${build_type} -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ${SRC_DIR}/${project} -DENABLE_FOOT=ON -DENABLE_FOOD=ON && make install || exit 1
        popd
    done
done
